package graywind.shop.service.impl;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import graywind.shop.bean.SessionData;
import graywind.shop.bean.User;
import graywind.shop.dao.UserMapper;
import graywind.shop.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean hasUser(User user) {
        User trueUser = userMapper.getUser(user.getUsername());
        if (trueUser.getPassword().equals(user.getPassword())) {
            return true;
        }
        return false;
    }

    @Override
    public void registerUser(User user) throws Exception {
        if (user == null || user.getUsername() == null || user.getPassword() == null) {
            throw new Exception("输出用户名或密码为空!");
        }
        User trueUser = userMapper.getUser(user.getUsername());
        if (trueUser != null) {
            throw new Exception("该用户名已存在!");
        }
        userMapper.register(user);
    }

    @Override
    public SessionData getSession(String session) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public User getUser(String username) {
        return userMapper.getUser(username);
    }

    @Override
    public void decreaseBalance(long userId, double amount) throws SQLException {
        User user = userMapper.getUserById(userId);
        if (user.getBalance() < amount) {
            throw new SQLException("余额不足");
        }
        userMapper.updateBanlance(userId, user.getBalance() - amount);
    }

    @Override
    public void increaseBanlance(long userId, double amount) throws SQLException {
        User user = userMapper.getUserById(userId);
        userMapper.updateBanlance(userId, user.getBalance() + amount);

    }
}
